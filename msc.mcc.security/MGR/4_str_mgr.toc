\contentsline {section}{\numberline {1}Wst\IeC {\k e}p}{4}
\contentsline {subsection}{\numberline {1.1}Motywacja}{4}
\contentsline {subsection}{\numberline {1.2}Cel pracy}{4}
\contentsline {section}{\numberline {2}Cloud computing}{5}
\contentsline {subsection}{\numberline {2.1}Mobile cloud computing}{5}
\contentsline {section}{\numberline {3}Aspekty bezpiecze\IeC {\'n}stwa w chmurze obliczeniowej}{6}
\contentsline {subsection}{\numberline {3.1}Warstwy bezpiecze\IeC {\'n}stwa}{6}
\contentsline {subsection}{\numberline {3.2}Znane problemy bezpiecze\IeC {\'n}stwa chmury obliczeniowej}{6}
\contentsline {subsection}{\numberline {3.3}Aktualne mechanizmy wykorzystywane w chmurze obliczeniowej}{7}
\contentsline {subsection}{\numberline {3.4}Problemy bezpiecze\IeC {\'n}stwa w mobilnej chmurze obliczeniowej}{8}
\contentsline {section}{\numberline {4}Teoria obietnic}{9}
\contentsline {subsection}{\numberline {4.1}Czym jest teoria obietnic?}{9}
\contentsline {subsection}{\numberline {4.2}Zarys historyczny teorii obietnic}{9}
\contentsline {subsection}{\numberline {4.3}CFEngine - teoria obietnic w praktyce}{9}
\contentsline {section}{\numberline {5}Architektura systemu}{10}
\contentsline {subsection}{\numberline {5.1}U\IeC {\.z}yte technologie}{11}
\contentsline {section}{\numberline {6}Opis rozwi\IeC {\k a}zania, specyfikacja}{12}
\contentsline {section}{\numberline {7}Testy}{13}
\contentsline {subsection}{\numberline {7.1}Por\IeC {\'o}wnanie z A}{13}
\contentsline {subsection}{\numberline {7.2}Por\IeC {\'o}wnanie z B}{13}
\contentsline {section}{\numberline {8}Podsumowanie i wnioski}{14}
\contentsline {section}{Spis rysunk\'ow}{15}
\contentsline {section}{\numberline {9}Bibliografia}{15}
