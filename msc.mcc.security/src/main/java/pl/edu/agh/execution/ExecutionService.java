package pl.edu.agh.execution;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;

import pl.edu.agh.communication.RequestWrapper;
import pl.edu.agh.configuration.RegisterService;
import pl.edu.agh.promises.PromiseConf;
import pl.edu.agh.promises.analyzers.PromiseAnalyzer;
import pl.edu.agh.promises.management.SlaveServer;
import pl.edu.agh.promises.management.TrustManager;
import pl.edu.agh.promises.management.exceptions.NoSlaveServerRegistered;
import pl.edu.agh.services.MasterServiceServer;
import pl.edu.agh.services.ServicesManager;

@Service
public class ExecutionService {
	
	private static final Logger logger = Logger.getLogger(ExecutionService.class);


	@Autowired
	private TrustManager trustManager;
	@Autowired
	private ServicesManager servicesManager;
	@Autowired
	private PromiseAnalyzer promiseAnalyzer;
	
	public String execute(RequestWrapper req, PromiseConf promiseConf) {
		return null;
	}
	
	public String analyzePromise(PromiseConf promiseConf) {
		return promiseAnalyzer.analyzePromise(promiseConf);
	}
	

	public String forwardToMasterService(RequestWrapper rw) throws NoSlaveServerRegistered {
		MasterServiceServer mss = servicesManager.getMasterServerFor(rw);
		return forwardRequest(mss, rw);
	}

	private String forwardRequest(MasterServiceServer mss, RequestWrapper rw) {
		// TODO Just send it
		return null;
	}


	public String executeOnSlave(RequestWrapper rw, PromiseConf extractedPromise) throws NoSlaveServerRegistered {
		SlaveServer bestSlave = trustManager.getBestSlave(rw, extractedPromise);	
		return sendRequestToSlave(bestSlave, rw, extractedPromise);
	}
	
	private String sendRequestToSlave(SlaveServer bestSlave, RequestWrapper rw,
			PromiseConf extractedPromise) {
		// TODO this is the best slave - send request and return response up
		return null;
	}

	public TrustManager getTrustManager() {
		return trustManager;
	}

	public void setTrustManager(TrustManager trustManager) {
		this.trustManager = trustManager;
	}

}
