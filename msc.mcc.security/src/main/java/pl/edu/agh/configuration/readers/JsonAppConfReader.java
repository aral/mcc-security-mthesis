package pl.edu.agh.configuration.readers;

import java.io.File;
import java.io.IOException;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import pl.edu.agh.configuration.Configuration;
import pl.edu.agh.configuration.IAppConfReader;
import pl.edu.agh.configuration.readers.exceptions.AppConfigurationFileNotFoundException;
import reactor.util.StringUtils;

import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class JsonAppConfReader implements IAppConfReader {

	private static final Logger logger = Logger.getLogger(JsonAppConfReader.class);

	private static final String CONF_JSON_KEY = "conf.json";
	private static final String DEFAULT_CONF_JSON_PATH = "src/main/resources/configuration/app_sample_conf.json";

	@Override
	public Configuration loadConfiguration() {
		String confFilePath = System.getProperty(CONF_JSON_KEY, "");
		if (!StringUtils.isEmpty(confFilePath)) {
			try {
				return readConfigurationFile(confFilePath);
			} catch (AppConfigurationFileNotFoundException e) {
				logger.error("Searched but not found in " + e.getMessage(), e);
			}
		}
		return Configuration.emptyConfiguration();
	}

	private Configuration readConfigurationFile(String confFilePath) throws AppConfigurationFileNotFoundException {
		File confFile = new File(confFilePath);
		ObjectMapper mp = new ObjectMapper();
		if (confFile.exists()) {
			try {
				return mp.readValue(confFile, Configuration.class);
			} catch (IOException e) {
				logger.error(e.getMessage(), e);
			}
		}
		File defaultConfFile = new File(DEFAULT_CONF_JSON_PATH);
		if (defaultConfFile.exists()) {
			try {
				return mp.readValue(defaultConfFile, Configuration.class);
			} catch (IOException e) {
				logger.error(e.getMessage(), e);
			}
		}
		throw new AppConfigurationFileNotFoundException(confFilePath, DEFAULT_CONF_JSON_PATH);
	}

}
