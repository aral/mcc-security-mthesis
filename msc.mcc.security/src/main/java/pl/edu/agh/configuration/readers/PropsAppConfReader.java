package pl.edu.agh.configuration.readers;

import org.springframework.stereotype.Component;

import pl.edu.agh.configuration.Configuration;
import pl.edu.agh.configuration.IAppConfReader;
import reactor.util.StringUtils;

@Component
public class PropsAppConfReader implements IAppConfReader {

	private static final String CONF_PROPS_KEY = "conf.props";

	@Override
	public Configuration loadConfiguration() {
		String confFilePath = System.getProperty(CONF_PROPS_KEY, "");
		if (!StringUtils.isEmpty(confFilePath)) {
			return readConfigurationFile(confFilePath);
		}
		return null;
	}

	private Configuration readConfigurationFile(String confFilePath) {
		return null;
	}

}
