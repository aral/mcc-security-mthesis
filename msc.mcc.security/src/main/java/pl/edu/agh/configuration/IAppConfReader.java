package pl.edu.agh.configuration;

public interface IAppConfReader {
	public Configuration loadConfiguration();
}
