package pl.edu.agh.configuration.readers.exceptions;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class AppConfigurationFileNotFoundException extends Exception {

	private static final long serialVersionUID = 4192658313739133454L;

	public AppConfigurationFileNotFoundException(String... searchedFiles) {
		super(ReflectionToStringBuilder.toString(searchedFiles,
				ToStringStyle.SHORT_PREFIX_STYLE));
	}
}
