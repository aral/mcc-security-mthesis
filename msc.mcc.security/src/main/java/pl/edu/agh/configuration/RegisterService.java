package pl.edu.agh.configuration;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import pl.edu.agh.configuration.exceptions.RegisterException;

@Service
public class RegisterService {

	private static final Logger logger = Logger.getLogger(RegisterService.class);

	public void registerInMaster(Configuration conf) throws RegisterException, HttpException, IOException {
		HttpClient httpClient = new HttpClient();
		int statusCode = httpClient.executeMethod(createRegisteringCall(conf));
		if (statusCode != HttpStatus.SC_OK) {
			throw new RegisterException();
		}
	}

	private HttpMethod createRegisteringCall(Configuration conf) {
		PostMethod pm = new PostMethod("http://" + conf.getMasterAddress() + ":" + conf.getMasterPort() + "/register");
		List<NameValuePair> body = new LinkedList<NameValuePair>();
		try {
			body.add(new NameValuePair("name", InetAddress.getLocalHost().getHostName()));
			body.add(new NameValuePair("address", InetAddress.getLocalHost().getHostAddress()));
			body.add(new NameValuePair("responsibility", conf.getResponsibility()));
		} catch (UnknownHostException e) {
			logger.error(e.getMessage(), e);
		}
		pm.setRequestBody((NameValuePair[]) body.toArray());
		return pm;
	}
}
