package pl.edu.agh.configuration;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "masterAddress", "masterPort", "responsibility" })
public class Configuration {

	@JsonProperty("masterAddress")
	private String masterAddress;
	@JsonProperty("masterPort")
	private String masterPort;
	@JsonProperty("responsibility")
	private String responsibility;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	public static Configuration emptyConfiguration() {
		return new Configuration();
	}

	/**
	 * 
	 * @return The masterAddress
	 */
	@JsonProperty("masterAddress")
	public String getMasterAddress() {
		return masterAddress;
	}

	/**
	 * 
	 * @param masterAddress
	 *            The masterAddress
	 */
	@JsonProperty("masterAddress")
	public void setMasterAddress(String masterAddress) {
		this.masterAddress = masterAddress;
	}

	/**
	 * 
	 * @return The masterPort
	 */
	@JsonProperty("masterPort")
	public String getMasterPort() {
		return masterPort;
	}

	/**
	 * 
	 * @param masterPort
	 *            The masterPort
	 */
	@JsonProperty("masterPort")
	public void setMasterPort(String masterPort) {
		this.masterPort = masterPort;
	}

	/**
	 * 
	 * @return The responsibility
	 */
	@JsonProperty("responsibility")
	public String getResponsibility() {
		return responsibility;
	}

	/**
	 * 
	 * @param responsibility
	 *            The responsibility
	 */
	@JsonProperty("responsibility")
	public void setResponsibility(String responsibility) {
		this.responsibility = responsibility;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}
