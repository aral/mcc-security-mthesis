package pl.edu.agh.configuration;

import java.io.IOException;

import javax.annotation.PostConstruct;

import org.apache.commons.httpclient.HttpException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import pl.edu.agh.configuration.exceptions.RegisterException;
import pl.edu.agh.promises.IPromiseConfReader;
import pl.edu.agh.promises.PromiseConf;

@Repository
public class AppConf {

	private static final Logger logger = Logger.getLogger(AppConf.class);

	private Configuration conf;
	private PromiseConf promiseConf;

	@Autowired
	@Qualifier(value = "jsonAppConfReader")
	private IAppConfReader reader;
	
	@Autowired
	@Qualifier(value = "jsonPromiseConfReader")
	private IPromiseConfReader promiseReader;

	@Autowired
	private RegisterService registerService;

	@PostConstruct
	public void init() {
		setConfiguration(reader.loadConfiguration());
		if (getConfiguration() == null) {
			setConfiguration(loadDefaultConf());
		}
		if (getConfiguration() != null) {
			try {
				if(!getConfiguration().getResponsibility().contains("front")){
					registerService.registerInMaster(getConfiguration());
				}
			} catch (RegisterException e) {
				logger.error(e.getMessage(), e);
			} catch (HttpException e) {
				logger.error(e.getMessage(), e);
			} catch (IOException e) {
				logger.error(e.getMessage(), e);
			}
		}
		setPromiseConf(promiseReader.loadConfiguration());
	}

	private Configuration loadDefaultConf() {
		return null;
	}

	public String getProperty(String name) {
		getConfiguration().getClass();
		return "";
	}

	public IAppConfReader getReader() {
		return reader;
	}

	public void setReader(IAppConfReader reader) {
		this.reader = reader;
	}

	public RegisterService getRegisterService() {
		return registerService;
	}

	public void setRegisterService(RegisterService registerService) {
		this.registerService = registerService;
	}

	public Configuration getConfiguration() {
		return conf;
	}

	public void setConfiguration(Configuration conf) {
		this.conf = conf;
	}

	public PromiseConf getPromiseConf() {
		return promiseConf;
	}

	public void setPromiseConf(PromiseConf promiseConf) {
		this.promiseConf = promiseConf;
	}
}
