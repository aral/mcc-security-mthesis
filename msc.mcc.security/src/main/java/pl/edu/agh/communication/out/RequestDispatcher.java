package pl.edu.agh.communication.out;

import java.util.LinkedList;
import java.util.List;

import org.apache.commons.httpclient.NameValuePair;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import pl.edu.agh.communication.RequestWrapper;
import pl.edu.agh.configuration.AppConf;
import pl.edu.agh.execution.ExecutionService;
import pl.edu.agh.promises.PromiseConf;
import pl.edu.agh.promises.management.SlaveServer;
import pl.edu.agh.promises.management.TrustManager;
import pl.edu.agh.promises.management.exceptions.NoSlaveServerRegistered;
import pl.edu.agh.services.MasterServiceServer;
import pl.edu.agh.services.ServicesManager;
import scala.NotImplementedError;

import com.fasterxml.jackson.databind.JsonNode;

@Component
public class RequestDispatcher {

	@Autowired
	private TrustManager trustManager;
	
	@Autowired
	private ServicesManager servicesManager;
	
	@Autowired
	private AppConf appConf;
	
	@Autowired
	private ExecutionService executionService;
	
	private static final Logger logger = Logger.getLogger(RequestDispatcher.class);
	private List<String> services = new LinkedList<String>();
	
	public String analyzePromise(JsonNode data){
		return executionService.analyzePromise(extractPromise(data));
	}

	public String dispatch(String sender, String serviceId, JsonNode data) throws NoSlaveServerRegistered {
		RequestWrapper rw = new RequestWrapper(sender, serviceId, data);
		if(appConf.getConfiguration().getResponsibility().equalsIgnoreCase(serviceId)){
			/* we're real service */
			return executionService.execute(rw, appConf.getPromiseConf());
		}else if(appConf.getConfiguration().getResponsibility().contains("front")){
			/* we're front controller - dispatch */
			return executionService.forwardToMasterService(rw);
		}else{
			/* we're master - analyzer promise, look for slave */
			return executionService.executeOnSlave(rw, extractPromise(data));
		}
	}

	private PromiseConf extractPromise(JsonNode data) {
		// TODO Auto-generated method stub
		return null;
	}

	public void addSlave(NameValuePair[] data) {
		for(NameValuePair nvp : data){
			if(nvp.getName().equalsIgnoreCase("responsibility")){
				if(nvp.getValue().contains("master")){
					MasterServiceServer mss = new MasterServiceServer(data);
					services.add(mss.getResponsibility());
					servicesManager.registerServer(mss);
				}else
				{
					trustManager.registerServer(new SlaveServer(data));
				}
				return;
			}
		}
	}

	public void removeSlave(NameValuePair[] data) {
		for(NameValuePair nvp : data){
			if(nvp.getName().equalsIgnoreCase("responsibility")){
				if(nvp.getValue().contains("master")){
					MasterServiceServer mss = new MasterServiceServer(data);
					services.remove(mss.getResponsibility());
					servicesManager.unregisterServer(mss);
				}else
				{
					trustManager.unregisterServer(new SlaveServer(data));
				}
				return;
			}
		}
	}

	public List<String> getAvailableServices() {
		return services;
	}

	public TrustManager getTrustManager() {
		return trustManager;
	}

	public void setTrustManager(TrustManager trustManager) {
		this.trustManager = trustManager;
	}

	public AppConf getAppConf() {
		return appConf;
	}

	public void setAppConf(AppConf appConf) {
		this.appConf = appConf;
	}

	public ExecutionService getExecutionService() {
		return executionService;
	}

	public void setExecutionService(ExecutionService executionService) {
		this.executionService = executionService;
	}

	public ServicesManager getServicesManager() {
		return servicesManager;
	}

	public void setServicesManager(ServicesManager servicesManager) {
		this.servicesManager = servicesManager;
	}
}
