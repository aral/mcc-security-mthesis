package pl.edu.agh.communication.in;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Response;

import org.apache.commons.httpclient.NameValuePair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import pl.edu.agh.communication.out.RequestDispatcher;
import pl.edu.agh.promises.management.exceptions.NoSlaveServerRegistered;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@RequestMapping(value = "/api", produces = MediaType.APPLICATION_JSON_VALUE)
public class RequestController {

	@Autowired
	private RequestDispatcher requestDispatcher;
	private ObjectMapper objectMapper = new ObjectMapper();

	@RequestMapping(value = "/services", method = RequestMethod.GET)
	public String getAvailableServices() throws JsonProcessingException {
		return objectMapper.writeValueAsString(requestDispatcher.getAvailableServices());
	}

	@RequestMapping(value = "/service/{serviceId}", consumes = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
	public String useService(@PathVariable(value = "serviceId") String serviceId, @RequestBody JsonNode data, HttpServletRequest request) throws NoSlaveServerRegistered {
		return requestDispatcher.dispatch(request.getRemoteUser(), serviceId, data);
	}
	
	@RequestMapping(value = "/promise", consumes = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
	public String checkPromise(@RequestBody JsonNode data, HttpServletRequest request) throws NoSlaveServerRegistered {
		return requestDispatcher.analyzePromise(data);
	}

	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public String registerSlave(@RequestBody NameValuePair[] data) {
		requestDispatcher.addSlave(data);
		return Response.ok().build().toString();
	}

	public RequestDispatcher getRequestDispatcher() {
		return requestDispatcher;
	}

	public void setRequestDispatcher(RequestDispatcher requestDispatcher) {
		this.requestDispatcher = requestDispatcher;
	}

}
