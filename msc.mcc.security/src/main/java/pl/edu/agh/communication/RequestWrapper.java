package pl.edu.agh.communication;

import com.fasterxml.jackson.databind.JsonNode;

public class RequestWrapper {
	private String requestName;
	private String sender;
	private JsonNode data;

	public RequestWrapper(String sender, String serviceId, JsonNode data) {
		this.sender = sender;
		this.requestName = serviceId;
		this.data = data;
	}

	public String getRequestName() {
		return requestName;
	}

	public void setRequestName(String requestName) {
		this.requestName = requestName;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public JsonNode getData() {
		return data;
	}

	public void setData(JsonNode data) {
		this.data = data;
	}
}
