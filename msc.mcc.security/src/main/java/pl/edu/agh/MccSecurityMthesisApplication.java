package pl.edu.agh;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;

import pl.edu.agh.configuration.AppConf;

@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan(basePackages = "pl.edu.agh.*")
public class MccSecurityMthesisApplication {

	public static void main(String[] args) {
		ApplicationContext ctx = SpringApplication.run(MccSecurityMthesisApplication.class, args);
		getResponsibility(ctx);
	}

	private static void getResponsibility(ApplicationContext ctx) {
		AppConf appConf = ctx.getBean(AppConf.class);
		appConf.init();
	}
}
