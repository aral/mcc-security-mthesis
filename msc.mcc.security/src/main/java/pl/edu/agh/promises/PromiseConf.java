package pl.edu.agh.promises;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "storageType", "filesEncryption", "dataReplication",
		"autoBackup" })
public class PromiseConf {

	@JsonProperty("storageType")
	private String storageType;
	@JsonProperty("filesEncryption")
	private String filesEncryption;
	@JsonProperty("dataReplication")
	private Boolean dataReplication;
	@JsonProperty("autoBackup")
	private Boolean autoBackup;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	public static PromiseConf emptyPromiseConf() {
		return new PromiseConf();
	}
	

	/**
	 * 
	 * @return The storageType
	 */
	@JsonProperty("storageType")
	public String getStorageType() {
		return storageType;
	}

	/**
	 * 
	 * @param storageType
	 *            The storageType
	 */
	@JsonProperty("storageType")
	public void setStorageType(String storageType) {
		this.storageType = storageType;
	}

	/**
	 * 
	 * @return The filesEncryption
	 */
	@JsonProperty("filesEncryption")
	public String getFilesEncryption() {
		return filesEncryption;
	}

	/**
	 * 
	 * @param filesEncryption
	 *            The filesEncryption
	 */
	@JsonProperty("filesEncryption")
	public void setFilesEncryption(String filesEncryption) {
		this.filesEncryption = filesEncryption;
	}

	/**
	 * 
	 * @return The dataReplication
	 */
	@JsonProperty("dataReplication")
	public Boolean getDataReplication() {
		return dataReplication;
	}

	/**
	 * 
	 * @param dataReplication
	 *            The dataReplication
	 */
	@JsonProperty("dataReplication")
	public void setDataReplication(Boolean dataReplication) {
		this.dataReplication = dataReplication;
	}

	/**
	 * 
	 * @return The autoBackup
	 */
	@JsonProperty("autoBackup")
	public Boolean getAutoBackup() {
		return autoBackup;
	}

	/**
	 * 
	 * @param autoBackup
	 *            The autoBackup
	 */
	@JsonProperty("autoBackup")
	public void setAutoBackup(Boolean autoBackup) {
		this.autoBackup = autoBackup;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}