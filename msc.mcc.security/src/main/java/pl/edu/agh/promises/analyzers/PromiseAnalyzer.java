package pl.edu.agh.promises.analyzers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import pl.edu.agh.configuration.AppConf;
import pl.edu.agh.promises.PromiseConf;
import pl.edu.agh.promises.executors.PromiseExecutor;

@Component
public class PromiseAnalyzer {
	
	@Autowired
	private PromiseExecutor promiseExecutor;
	
	@Autowired
	private AppConf appConf;

	public String analyzePromise(PromiseConf promiseConf) {
		return promiseExecutor.executePromise(promiseConf	, appConf.getPromiseConf()).toString();
	}

	public PromiseExecutor getPromiseExecutor() {
		return promiseExecutor;
	}

	public void setPromiseExecutor(PromiseExecutor promiseExecutor) {
		this.promiseExecutor = promiseExecutor;
	}

	public AppConf getAppConf() {
		return appConf;
	}

	public void setAppConf(AppConf appConf) {
		this.appConf = appConf;
	}

}
