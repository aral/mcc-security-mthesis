package pl.edu.agh.promises.analyzers;

import org.springframework.stereotype.Component;

import pl.edu.agh.promises.PromiseConf;

@Component
public class PromiseComparer {
	public boolean comparePromises(PromiseConf expected, PromiseConf current){
		return expected.equals(current);
	}
}
