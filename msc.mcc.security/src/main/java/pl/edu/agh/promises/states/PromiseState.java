package pl.edu.agh.promises.states;

public enum PromiseState {
	KEPT(1), NOT_KEPT(-1), ADAPTED(0);
	
	private int trustValue;
	
	private PromiseState(int trustValue){
		this.trustValue = trustValue;
	}
	
	public int getTrustValue(){
		return this.trustValue;
	}
}
