package pl.edu.agh.promises.management;

import java.util.Map;
import java.util.TreeMap;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;

import pl.edu.agh.communication.RequestWrapper;
import pl.edu.agh.promises.PromiseConf;
import pl.edu.agh.promises.management.exceptions.NoSlaveServerRegistered;

@Service
public class TrustManager {

	private static final Logger logger = Logger.getLogger(TrustManager.class);

	private final static int MIN_TRUST = 0;
	private final static int THRESHOLD_TRUST = 30;
	private final static int INITIAL_TRUST = 50;
	private final static int MAX_TRUST = 100;

	private Map<SlaveServer, Integer> slaves = new TreeMap<SlaveServer, Integer>();

	public boolean registerServer(SlaveServer ss) {
		if (!slaves.keySet().contains(ss)) {
			slaves.put(ss, INITIAL_TRUST);
			return true;
		}
		return false;
	}

	public boolean unregisterServer(SlaveServer ss) {
		if (slaves.containsKey(ss)) {
			slaves.remove(ss);
			return true;
		}
		return false;
	}

	public SlaveServer getBestSlave(RequestWrapper request, PromiseConf promiseConf) throws NoSlaveServerRegistered {
		//TODO got request and requested promise conf - send to slaves (promise as json) and return the best one
		if (!slaves.isEmpty()) {
			return slaves.keySet().iterator().next();
		}
		logger.info("Trust manager doesn't have any underlying slave servers");
		throw new NoSlaveServerRegistered();
	}

}
