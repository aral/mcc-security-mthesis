package pl.edu.agh.promises.management;

import org.apache.commons.httpclient.NameValuePair;


public class SlaveServer {

	private String name;
	private String address;
	private int port;

	public SlaveServer(NameValuePair[] data){
		for(NameValuePair nvp : data){
			if(nvp.getName().equalsIgnoreCase("name")){
				this.name = nvp.getValue();
			}
			if(nvp.getName().equalsIgnoreCase("address")){
				this.address = nvp.getValue();
			}
			if(nvp.getName().equalsIgnoreCase("port")){
				this.port = Integer.valueOf(nvp.getValue());
			}			
		}
	}
	
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + port;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SlaveServer other = (SlaveServer) obj;
		if (address == null) {
			if (other.address != null)
				return false;
		} else if (!address.equals(other.address))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (port != other.port)
			return false;
		return true;
	}
}
