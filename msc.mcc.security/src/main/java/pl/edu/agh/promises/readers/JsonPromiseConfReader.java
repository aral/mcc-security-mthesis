package pl.edu.agh.promises.readers;

import java.io.File;
import java.io.IOException;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;

import pl.edu.agh.configuration.Configuration;
import pl.edu.agh.configuration.readers.JsonAppConfReader;
import pl.edu.agh.configuration.readers.exceptions.AppConfigurationFileNotFoundException;
import pl.edu.agh.promises.IPromiseConfReader;
import pl.edu.agh.promises.PromiseConf;
import reactor.util.StringUtils;

@Component
public class JsonPromiseConfReader implements IPromiseConfReader {

	private static final Logger logger = Logger.getLogger(JsonPromiseConfReader.class);

	private static final String CONF_JSON_KEY = "promise.json";
	private static final String DEFAULT_CONF_JSON_PATH = "src/main/resources/configuration/promise_sample_conf.json";
	
	@Override
	public PromiseConf loadConfiguration() {
		String confFilePath = System.getProperty(CONF_JSON_KEY, "");
		if (!StringUtils.isEmpty(confFilePath)) {
			try {
				return readConfigurationFile(confFilePath);
			} catch (AppConfigurationFileNotFoundException e) {
				logger.error("Searched but not found in " + e.getMessage(), e);
			}
		}
		return PromiseConf.emptyPromiseConf();
	}
	
	private PromiseConf readConfigurationFile(String confFilePath) throws AppConfigurationFileNotFoundException {
		File confFile = new File(confFilePath);
		ObjectMapper mp = new ObjectMapper();
		if (confFile.exists()) {
			try {
				return mp.readValue(confFile, PromiseConf.class);
			} catch (IOException e) {
				logger.error(e.getMessage(), e);
			}
		}
		File defaultConfFile = new File(DEFAULT_CONF_JSON_PATH);
		if (defaultConfFile.exists()) {
			try {
				return mp.readValue(defaultConfFile, PromiseConf.class);
			} catch (IOException e) {
				logger.error(e.getMessage(), e);
			}
		}
		throw new AppConfigurationFileNotFoundException(confFilePath, DEFAULT_CONF_JSON_PATH);
	}

}
