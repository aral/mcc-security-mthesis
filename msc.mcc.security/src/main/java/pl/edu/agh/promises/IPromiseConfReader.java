package pl.edu.agh.promises;

public interface IPromiseConfReader {
	public PromiseConf loadConfiguration();
}
