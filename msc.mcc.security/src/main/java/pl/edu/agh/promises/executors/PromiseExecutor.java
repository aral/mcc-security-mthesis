package pl.edu.agh.promises.executors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pl.edu.agh.promises.PromiseConf;
import pl.edu.agh.promises.analyzers.PromiseComparer;
import pl.edu.agh.promises.states.PromiseState;

@Service
public class PromiseExecutor {
	
	@Autowired
	private PromiseComparer promiseComparer;
		
	public PromiseState executePromise(PromiseConf promiseToKeep, PromiseConf currentPromiseConf){
		if(promiseComparer.comparePromises(promiseToKeep, currentPromiseConf)){
			return PromiseState.KEPT;
		}else if(adaptPromise(promiseToKeep)){
			return PromiseState.ADAPTED;
		}else{
			return PromiseState.NOT_KEPT;
		}
	}

	private boolean adaptPromise(PromiseConf promiseToKeep) {
		// TODO Adapt current server configuration to keep the promise
		return false;
	}

	public PromiseComparer getPromiseComparer() {
		return promiseComparer;
	}

	public void setPromiseComparer(PromiseComparer promiseComparer) {
		this.promiseComparer = promiseComparer;
	}

}
