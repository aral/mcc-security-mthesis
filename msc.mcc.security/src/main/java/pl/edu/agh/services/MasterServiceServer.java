package pl.edu.agh.services;

import org.apache.commons.httpclient.NameValuePair;

public class MasterServiceServer {
	private String name;
	private String address;
	private int port;
	private String responsibility;

	public MasterServiceServer(NameValuePair[] data){
		for(NameValuePair nvp : data){
			if(nvp.getName().equalsIgnoreCase("name")){
				this.name = nvp.getValue();
			}
			if(nvp.getName().equalsIgnoreCase("address")){
				this.address = nvp.getValue();
			}
			if(nvp.getName().equalsIgnoreCase("port")){
				this.port = Integer.valueOf(nvp.getValue());
			}
			if(nvp.getName().equalsIgnoreCase("responsibility")){
				this.responsibility = nvp.getValue();
			}		
		}
	}
	
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getPort() {
		return port;
	}


	public String getResponsibility() {
		return responsibility;
	}


	public void setResponsibility(String responsibility) {
		this.responsibility = responsibility;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + port;
		result = prime * result
				+ ((responsibility == null) ? 0 : responsibility.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MasterServiceServer other = (MasterServiceServer) obj;
		if (address == null) {
			if (other.address != null)
				return false;
		} else if (!address.equals(other.address))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (port != other.port)
			return false;
		if (responsibility == null) {
			if (other.responsibility != null)
				return false;
		} else if (!responsibility.equals(other.responsibility))
			return false;
		return true;
	}
}
