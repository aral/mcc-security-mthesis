package pl.edu.agh.services;

import java.util.LinkedList;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import pl.edu.agh.communication.RequestWrapper;
import pl.edu.agh.promises.management.exceptions.NoSlaveServerRegistered;

@Component
public class ServicesManager {
	private static final Logger logger = Logger
			.getLogger(ServicesManager.class);

	private List<MasterServiceServer> services = new LinkedList<MasterServiceServer>();

	public boolean registerServer(MasterServiceServer mss) {
		if (!services.contains(mss)) {
			services.add(mss);
			return true;
		}
		return false;
	}

	public boolean unregisterServer(MasterServiceServer mss) {
		if (services.contains(mss)) {
			services.remove(mss);
			return true;
		}
		return false;
	}

	public MasterServiceServer getMasterServerFor(RequestWrapper request)
			throws NoSlaveServerRegistered {
		if (!services.isEmpty()) {
			for (MasterServiceServer mss : services) {
				if (mss.getResponsibility().equalsIgnoreCase(
						request.getRequestName())) {
					return mss;
				}
			}
		}
		logger.info("Trust manager doesn't have any underlying slave servers");
		throw new NoSlaveServerRegistered();
	}
}
